import processing.video.*;

void setup() {
  size(512, 412, P2D);
  loadReplayM1("Sadness.mp4");
}

void draw() {
  background(0);
  drawReplay();
}
Movie m1;

void loadReplayM1(String s) {
  m1 = new Movie(this, s);
  m1.loop();
}

void movieEvent(Movie m) {
  m.read();
}

void drawReplay() {
  // println("drawing replay");
  image(m1, 0, 0, width, height);
}

void drawReplay(String j, String k) {
  // take filenames [j, k] and mix them together
  println("drawing replay with two types");
}

void drawReplay(String j, String k, String l ) {
  // take filenames [j, k, l] and mix them together
  println("drawing replay with three types");
}

void stopReplay() {
  // functions for stopping playback for saving background memory
}

JSONArray dictionary;
color[] keywordColors;
String[] keywordDictionary;

void setupKeywordVariables() {
  dictionary = loadJSONArray("data/dict.json");
  keywordColors = new color[dictionary.size()];
  keywordDictionary = new String[dictionary.size()];
}

void setupKeywordButtons() {
  for (int i = 0; i < dictionary.size(); i++) {
    JSONObject keywordTopic = dictionary.getJSONObject(i);
    String word = keywordTopic.getString("keyword");

    keywordDictionary[i] = word;
    keywordColors[i] = parseStringToColor(keywordTopic.getString("color"));
    logKeywordColors(i);

    createButtons(word, i);
  }
}

color parseStringToColor(String s) {
  String ff = "FF" + s.substring(1);
  return color(unhex(ff));
}

// String[] getVideosFromDatabase(String keyword) in videoDatabase.pde
color getColorFromDictionary(String keyword) {
  for (int i = 0; i < dictionary.size(); i++) {
    JSONObject promptObject = dictionary.getJSONObject(i);
    String word = promptObject.getString("keyword");

    if (match(word, keyword) != null) {
      // logFoundKeyword(true, promptObject.getString("color"));
      return parseStringToColor(promptObject.getString("color"));
    }
  }

  // if the keyword is not found in the dictionary
  logFoundKeyword(false, keyword);
  return -1;
}

// ideally you'd be sending this the array of current loadedKeywordVideos
// length of _ka will determine the length of the color[] returned.
color[] getColorsFromDictionary(String[] _ka) {
  color[] _cr = new color[_ka.length];
  for (int i = 0; i < _ka.length; i++) {
    _cr[i] = getColorFromDictionary(_ka[i]);
  }
  return _cr;
}

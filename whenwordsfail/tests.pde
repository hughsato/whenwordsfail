// min / max measuring
float min = 0.;
float max = 0.;
void minMax(float k) {
  if (k > max) {
    max = k;
  } else if (k < min) {
    min = k;
  }
  println("min: ", min, "max: ", max);
}
import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.video.*; 
import com.hamoid.*; 
import org.openkinect.processing.*; 
import controlP5.*; 
import codeanticode.syphon.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class whenwordsfail extends PApplet {






// ---- KINECT2 VARIABLES ---- //
Kinect2 kinect2;
PGraphics canvas;
PImage depthImg;
int minDepth = 100;
int maxDepth = 3100;

final int MOVIE_COUNT = 3;
final int RECORD_LENGTH = 10000;
final int FPS = 30;

// Initial SceneState
SceneState sceneState = SceneState.SELECTION;

// ---- RECORDING ---- //
VideoExport videoExport;
String filename = "";

// ---- SETTINGS ---- //
public void settings() {
  size(1024, 824);
  // PJOGL.profile=2;
  // fullScreen();
}

// ---- SETUP ---- //
public void setup() {
  frameRate(FPS);
  cursor(CROSS);

  interfaceSetup();
  loadVideoDatabase();
  if (KinectConfig.show) setupKinect(); else setupCamera();
  // setupSyphonServer(); // this can only run if kinect2 is running

  setupRecording();
  setupMovies();
}

// ---- DRAW ---- //
public void draw() {
  background(0);

  switch(sceneState) {
  case SELECTION:
    tint(0, 0);
    if (KinectConfig.show) drawKinect(); else drawCamera();
    interfaceIsVisible = true;
    drawInterface();
    break;

  // NEXT: eventKeywordSelect() in events.pde
  case READY:
    if (KinectConfig.show) drawKinect(); else drawCamera();
    // TODO: draw "Recording for _______" label

    drawGoButton();
    drawTimer();
    break;

  // NEXT: eventGoSelect() -> beginCapture()
  case RECORDING:
    //  is called by event handler
    if (KinectConfig.show) drawKinect(); else drawCamera();
    drawTimer();

    if (captureFrame() < 0) {
      sceneState = SceneState.REPLAYSINGLE;
      loadReplayM1(lastRecordingFilename);
      replayTime = millis();
    }
    break;

  case REPLAYSINGLE:
    drawReplay(0, 255, keywordsLoaded[0]);
    // also could draw a label for what keyword you're on.

    if (replayTimer() < 0) {
      sceneState = SceneState.SELECTION;
      loadKeywordMovies(latestKeyword, 1);
      replayTime = millis();
      break;
    }
    break;

  case REPLAYDOUBLE:
    for (int i = 0; i < 2; i++){
      drawReplay(i, 200, keywordsLoaded[i]);
    }

    if (replayTimer() < 0) {
      sceneState = SceneState.REPLAYTRIPLE;
      loadKeywordMovies(latestKeyword, 2);
      replayTime = millis();
      break;
    }
    break;

  case REPLAYTRIPLE:
    for (int i = 0; i < movies.length; i++){
      drawReplay(i, 200, keywordsLoaded[i]);
    }
    blendMode(ADD);

    if (replayTimer() < 0) {
      sceneState = SceneState.LIVEPLAY;
      replayTime = millis();
      break;
    }
    break;

  case LIVEPLAY:
    for (int i = 0; i < movies.length; i++){
      drawReplay(i, 200, keywordsLoaded[i]);
    }
    blendMode(ADD);

    tint(255, 255);
    if (KinectConfig.show) drawKinect(); else drawCamera();
    break;

  default:
    println("sceneId does not exist");
    break;
  }

  drawCalibration();
  stroke(0);
}
enum SceneState {
  SELECTION,
  READY,
  RECORDING,
  REPLAYSINGLE,
  REPLAYDOUBLE,
  REPLAYTRIPLE,
  LIVEPLAY
  ;
}
 

 SyphonServer server;

 public void setupSyphonServer() {
   server = new SyphonServer(this, "Processing Syphon");
 }

 public void sendToSyphon(PGraphics pg) {
   server.sendImage(pg);
 }
Capture cam;

public void setupCamera() {
  String[] cameras = Capture.list();
  printArray(cameras);
  cam = new Capture(this, cameras[9]);
  cam.start();
}

public void drawCamera() {
  if (cam.available() == true) {
    cam.read();
  }
  image(cam, 0, 0, width, height);
}
interface KinectConfig {
  boolean show = false;
}
public void controlEvent(ControlEvent theEvent) {
  String eventName = theEvent.getName();

  // ---- TRIGGERS HENCEFORTH ---- >>
  switch(eventName) {
    case "go":
      eventGoSelect();
      break;

    case "timer":
      break;

    default:
      eventKeywordSelect(eventName);
      break;
  }
}

public void eventGoSelect() {
  // switch to recording mode
  sceneState = SceneState.RECORDING;
  beginCapture();

  hideGo();
  showTimer();
}

public void eventKeywordSelect(String eventName) {
  // All keyword selections go here
  setOutputFilename(eventName);
  logRecordState(eventName);

  sceneState = SceneState.READY;

  hideInterface();
  showGo();
}
ControlP5 cp5;
ControlP5 go;
ControlP5 timer;

final int
SLIDER_WIDTH = 800,
SLIDER_HEIGHT = 100,
BUTTON_WIDTH = 200,
BUTTON_HEIGHT = 70,
BUTTON_START_XPOS = 300,
BUTTON_START_YPOS = 100;

final String
FONT_PATH = "../data/AvenirNext-Bold-40.vlw";

// ---- STYLING ---- //
PFont pfont;
ControlFont font;

// ---- VISIBILITY ---- //
boolean interfaceIsVisible = false;
boolean goIsVisible = false;
boolean timerIsVisible = false;

public void interfaceSetup() {
  cp5 = new ControlP5(this);
  go = new ControlP5(this);
  timer = new ControlP5(this);

  // set font
  pfont = loadFont(FONT_PATH);
  font = new ControlFont(pfont,25);

  // keywordDatabase.pde
  setupKeywordVariables();
  setupKeywordButtons();

  go.addButton("go", 0)
    .setColorBackground(0xff00aa00)
    .setSize(BUTTON_WIDTH, BUTTON_HEIGHT)
    .setPosition(width/2-BUTTON_WIDTH/2, height - 200)
    .setFont(font)
    .setColorForeground(color(255))
    .setColorCaptionLabel(50)
    ;
  go.hide();

  timer.addSlider("timer")
    .setPosition(width/2-SLIDER_WIDTH/2, height-20)
    .setWidth(SLIDER_WIDTH)
    .setRange(0, RECORD_LENGTH)
    .setValue(0)
    .setSize(SLIDER_WIDTH, SLIDER_HEIGHT)
    .setColorForeground(0xff2ff97d)
    ;
  timer.hide();

  loadedLog("interface");
}

public void createButtons(String w, int i) {
  cp5.addButton(w, i)
    .setPosition(width/2-BUTTON_WIDTH/2,BUTTON_START_YPOS + i*(10+BUTTON_HEIGHT))
    .setColorBackground(keywordColors[i])
    .setSize(BUTTON_WIDTH, BUTTON_HEIGHT)
    .linebreak()
    .setFont(font)
    .setColorForeground(color(255))
    .setColorCaptionLabel(50)
    ;
}

public void drawInterface() {
  if (interfaceIsVisible) {
    interfaceVisibility(true);
  } else {
    interfaceVisibility(false);
  }
}

public void interfaceVisibility(boolean visInt) {
  if (visInt) {
    cp5.show();
  } else {
    cp5.hide();
  }
}

public void showInterface() {
  interfaceIsVisible = true;
  drawInterface();
}

public void hideInterface() {
  interfaceIsVisible = false;
  drawInterface();
}

public void drawGoButton() {
  if (goIsVisible) {
    go.show();
  } else {
    go.hide();
  }
}

public void showGo() {
  goIsVisible = true;
  drawGoButton();
}

public void hideGo() {
  goIsVisible = false;
  drawGoButton();
}

public void drawTimer() {
  if (timerIsVisible) {
    timer.show();
  } else {
    timer.hide();
  }
}

public void hideTimer() {
  timerIsVisible = false;
  drawTimer();
}

public void showTimer() {
  timerIsVisible = true;
  drawTimer();
}

public void updateTimer(int t) {
  timer.getController("timer")
    .setValue(t);
}

public void drawCalibration() {
  if (showCalibration && kinect2 != null) {
    fill(255);
    text("THRESHOLD: [" + minDepth + ", " + maxDepth + "]", 10, kinect2.depthHeight);
  }
}
// ---- Public Variables ---- //
// public void interfaceVisibility(boolean)

boolean showCalibration = false;
boolean debugGo = false;

public void keyPressed() {
  // Adjust the angle and the depth threshold min and max
  if (key == 'a') {
    if (showCalibration) minDepth = constrain(minDepth+100, 0, maxDepth);
    else loadKeywordMovies("Resilience", PApplet.parseInt(random(movies.length)));
  } else if (key == 's') {
    if (showCalibration) minDepth = constrain(minDepth-100, 0, maxDepth);
    else loadKeywordMovies("Resistance", PApplet.parseInt(random(movies.length)));
  } else if (key == 'z') {
    maxDepth = constrain(maxDepth+100, minDepth, 1165952918);
  } else if (key =='x') {
    maxDepth = constrain(maxDepth-100, minDepth, 1165952918);
  } else if (key == 'c') {
    // Show Kinect2 Calibration
    showCalibration = !showCalibration;
  } else if (key == '8') {
    interfaceIsVisible = false;
  } else if (key == '9') {
    interfaceIsVisible = true;
  } else if (key == '1') {
    loadNewMovie(0);
  } else if (key == '2') {
    loadNewMovie(1);
  } else if (key == '3') {
    loadNewMovie(2);
  } else if (key == 'k') {
    if (sceneState != SceneState.LIVEPLAY) {
      sceneState = SceneState.LIVEPLAY;
      hideInterface();
      hideTimer();
    } else {
      sceneState = SceneState.SELECTION;
      showInterface();
      showTimer();
    }
  } else if (key == 'l') {
    loadNewMovies();
  } else if (key == 'q') {
    loadMoviesOfKeyword("Resilience");
  } else if (key == 'w') {
    loadMoviesOfKeyword("Resistance");
  } else if (key == 'e') {
    loadMoviesOfKeyword("Freedom");
  } else if (key == 'r') {
    loadMoviesOfKeyword("Democracy");
  } else if (key == 't') {
    loadMoviesOfKeyword("Revolution");
  } else if (key == 'y') {
    loadMoviesOfKeyword("Change");
  } else if (key == 'u') {
    loadMoviesOfKeyword("Citizen");
  } else if (key == 'd') {
    loadKeywordMovies("Freedom", PApplet.parseInt(random(movies.length)));
  } else if (key == 'f') {
    loadKeywordMovies("Democracy", PApplet.parseInt(random(movies.length)));
  } else if (key == 'g') {
    loadKeywordMovies("Revolution", PApplet.parseInt(random(movies.length)));
  } else if (key == 'h') {
    loadKeywordMovies("Change", PApplet.parseInt(random(movies.length)));
  } else if (key == 'j') {
    loadKeywordMovies("Citizen", PApplet.parseInt(random(movies.length)));
  }
}
JSONArray dictionary;
int[] keywordColors;
String[] keywordDictionary;

public void setupKeywordVariables() {
  dictionary = loadJSONArray("data/dict.json");
  keywordColors = new int[dictionary.size()];
  keywordDictionary = new String[dictionary.size()];
}

public void setupKeywordButtons() {
  for (int i = 0; i < dictionary.size(); i++) {
    JSONObject keywordTopic = dictionary.getJSONObject(i);
    String word = keywordTopic.getString("keyword");

    keywordDictionary[i] = word;
    keywordColors[i] = parseStringToColor(keywordTopic.getString("color"));
    logKeywordColors(i);

    createButtons(word, i);
  }
}

public int parseStringToColor(String s) {
  String ff = "FF" + s.substring(1);
  return color(unhex(ff));
}

// String[] getVideosFromDatabase(String keyword) in videoDatabase.pde
public int getColorFromDictionary(String keyword) {
  for (int i = 0; i < dictionary.size(); i++) {
    JSONObject promptObject = dictionary.getJSONObject(i);
    String word = promptObject.getString("keyword");

    if (match(word, keyword) != null) {
      // logFoundKeyword(true, promptObject.getString("color"));
      return parseStringToColor(promptObject.getString("color"));
    }
  }

  // if the keyword is not found in the dictionary
  logFoundKeyword(false, keyword);
  return -1;
}

// ideally you'd be sending this the array of current loadedKeywordVideos
// length of _ka will determine the length of the color[] returned.
public int[] getColorsFromDictionary(String[] _ka) {
  int[] _cr = new int[_ka.length];
  for (int i = 0; i < _ka.length; i++) {
    _cr[i] = getColorFromDictionary(_ka[i]);
  }
  return _cr;
}
public void setupKinect() {
  kinect2 = new Kinect2(this);
  kinect2.initDepth();
  kinect2.initDevice();
  depthImg = new PImage(kinect2.depthWidth, kinect2.depthHeight);
  canvas = createGraphics(kinect2.depthWidth, kinect2.depthHeight, P2D);
}

public void drawKinect() {
  int[] rawDepth = kinect2.getRawDepth();

  // Threshold the depth image
  for (int i=0; i < rawDepth.length; i++) {
    if (rawDepth[i] >= minDepth && rawDepth[i] <= maxDepth) {
      // raw depth goes up to around 1200
      depthImg.pixels[i] = color(map(rawDepth[i], minDepth, maxDepth, 255, 0));
      //minMax(rawDepth[i]); // for testing
    } else {
      depthImg.pixels[i] = color(0);
    }
  }

  // Draw the thresholded image
  depthImg.updatePixels();
  canvas.beginDraw();
  canvas.background(0);
  canvas.image(depthImg, 0, 0);
  canvas.endDraw();
  //sendToSyphon(canvas);

  tint(255, 255); // must be reset here
  image(canvas, 0, 0, width, height);
}
public void logKeywordColors(int k) {
  println("keywordcolor loaded: " + keywordColors[k]);
}

public void logFoundKeyword(boolean b, String str) {
  if (b) {
    println("found: " + str);
  } else {
    println("not found: " + str);
  }
}

public void logTableCount(int videoCount) {
  println("We now have " + videoCount + " videos");
}

public void logError(String error) {
  println(error);
}

public void logMovieFileLoaded(String k, int whichMov){
  println("database loaded: " + k + whichMov);
}

public void logMovieState(String state) {
  println(state);
}

public void logRecordState(String state) {
  println("ready to record for: " + state);
}

public void logEventName(String event) {
  println("got a control event from controller with name " + event);
}

public void loadedLog(String loaded) {
  println("loaded: " + loaded);
}
// ---- Public Variables ---- //
// VideoExport videoExport;
// String filename = "";

int recordTime;
String lastRecordingFilename = "";
String latestKeyword = "";

public void setupRecording() {
  videoExport = new VideoExport(this);
  videoExport.setDebugging(true);
  recordTime = 0;
}

public void setOutputFilename(String s) {
  lastRecordingFilename =  s + millis() + ".mp4";
  videoExport.setMovieFileName("data/videos/" + lastRecordingFilename);

  // ---- ADD FILE TO DATABASE ---- //
  addVideoToDatabase(lastRecordingFilename, s);
  latestKeyword = s;
}

// ---- START RECORDING ---- //
public void beginCapture() {
  videoExport.startMovie();
  recordTime = millis();
  logMovieState("Started movie.");
}

// ---- CAPTURE FRAMES + GOAL CHECK ---- //
public int captureFrame() {
  videoExport.saveFrame();
  updateTimer(millis() - recordTime);

  // ---- CASE: STOP RECORDING ----//
  if (millis() > recordTime + RECORD_LENGTH) {
    endCapture();
    return -1;
  }
  return 0;
}

// ---- END RECORDING ---- //
public void endCapture() {
  videoExport.endMovie();
  logMovieState("Finished movie.");
  hideTimer();
}
// min / max measuring
float min = 0.f;
float max = 0.f;
public void minMax(float k) {
  if (k > max) {
    max = k;
  } else if (k < min) {
    min = k;
  }
  println("min: ", min, "max: ", max);
}
Table table;
int INIT_ARR_LENGTH = 0;

public void loadVideoDatabase() {
  // table has headers filename,keyword
  table = loadTable("videoDatabase.csv", "header");
  loadedLog("video database");
}

public void addVideoToDatabase(String f, String k) {
  TableRow row = table.addRow();
  row.setString("filename", f);
  row.setString("keyword", k);
  logTableCount(table.getRowCount());

  saveTable(table, "data/videoDatabase.csv");
  loadVideoDatabase();
}

public String[] getVideosFromDatabase() {
  // this would return all videos into the string[]
  String[] sr = new String[INIT_ARR_LENGTH];
  for (TableRow row : table.matchRows("*", "keyword")) {
    // println(row.getString("filename"));
    sr = append(sr, row.getString("filename"));
  }
  return sr;
}

// color[] getColorsFromDictionary(String _k) in keywordDatabase.pde
public String[] getVideosFromDatabase(String _k) {
  // this returns only videos of a specific keyword
  String[] sr = new String[INIT_ARR_LENGTH];
  for (TableRow row : table.findRows(_k, "keyword")) {
    // println("found " + row.getString("filename"));
    sr = append(sr, row.getString("filename"));
  }
  return sr;
}
Movie[] movies = new Movie[MOVIE_COUNT];
int movieColors[] = new int[MOVIE_COUNT];

int replayTime = 0;
int replayTimeLength = 20000;

// Stores keywords loaded in m1, m2, m3 Movie objects
String keywordsLoaded[] = {"", "", ""};
String VIDEO_PATH = "../videos/";

public void setupMovies() {
  for (int i = 0; i < movies.length; i++){
    loadKeywordMovies(keywordDictionary[(int)random(keywordDictionary.length)], i);
  }
}

// rename this function!
public void loadReplayM1(String s) {
  // keywordsLoaded[0] = s;
  movies[0] = new Movie(this, VIDEO_PATH + s);
  movies[0].loop();
}

// provide the keyword to randomly find one and which movie to load it to.
public void loadKeywordMovies(String k, int whichMov) {
  String[] keywordVideos = getVideosFromDatabase(k);
  int keywordColor = getColorFromDictionary(k);

  // selecting random index from available videos for that keyword
  int l = (int)random(keywordVideos.length);
  logMovieFileLoaded(k, whichMov);

  keywordsLoaded[whichMov] = k;
  movieColors[whichMov] = keywordColor;

  movies[whichMov] = new Movie(this, VIDEO_PATH + keywordVideos[l]);
  movies[whichMov].loop();
}

public void loadMoviesOfKeyword(String keyword) {
  for (int i = 0; i < movies.length; i++) {
    movies[i].stop();
    loadKeywordMovies(keyword, i);
    movies[i].play();
  }
}

public void drawReplay(int whichMov, int t, String k) {
  int c = getColorFromDictionary(k);
  tint(c, t);
  image(movies[whichMov], 0, 0, width, height);
}

public int replayTimer() {
  int timeLeft = replayTime + replayTimeLength - millis();
  println("time left: " + timeLeft);

  if (millis() > replayTime + replayTimeLength) {
    return -1;
  }
  return 0;
}

public void loadNewMovie(int whichMov) {
    movies[whichMov].stop();
    movies[whichMov] = null;

    loadKeywordMovies(keywordDictionary[(int)random(keywordDictionary.length)], whichMov);
    movies[whichMov].play();
}

public void loadNewMovies() {
    stopReplay();
    for (int i = 0; i < movies.length; i++){
      loadKeywordMovies(keywordDictionary[(int)random(keywordDictionary.length)], i);
    }
    for (Movie mm : movies) {
      mm.play();
    }
}

public void stopReplay() {
  for (Movie mm : movies) {
    mm.stop();
    mm = null;
  }
}

public void movieEvent(Movie m) {
  m.read();
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "whenwordsfail" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}

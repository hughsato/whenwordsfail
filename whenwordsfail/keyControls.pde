// ---- Public Variables ---- //
// public void interfaceVisibility(boolean)

boolean showCalibration = false;
boolean debugGo = false;

void keyPressed() {
  // Adjust the angle and the depth threshold min and max
  if (key == 'a') {
    if (showCalibration) minDepth = constrain(minDepth+100, 0, maxDepth);
    else loadKeywordMovies("Resilience", int(random(movies.length)));
  } else if (key == 's') {
    if (showCalibration) minDepth = constrain(minDepth-100, 0, maxDepth);
    else loadKeywordMovies("Resistance", int(random(movies.length)));
  } else if (key == 'z') {
    maxDepth = constrain(maxDepth+100, minDepth, 1165952918);
  } else if (key =='x') {
    maxDepth = constrain(maxDepth-100, minDepth, 1165952918);
  } else if (key == 'c') {
    // Show Kinect2 Calibration
    showCalibration = !showCalibration;
  } else if (key == '8') {
    interfaceIsVisible = false;
  } else if (key == '9') {
    interfaceIsVisible = true;
  } else if (key == '1') {
    loadNewMovie(0);
  } else if (key == '2') {
    loadNewMovie(1);
  } else if (key == '3') {
    loadNewMovie(2);
  } else if (key == 'k') {
    if (sceneState != SceneState.LIVEPLAY) {
      sceneState = SceneState.LIVEPLAY;
      hideInterface();
      hideTimer();
    } else {
      sceneState = SceneState.SELECTION;
      showInterface();
      showTimer();
    }
  } else if (key == 'l') {
    loadNewMovies();
  } else if (key == 'q') {
    loadMoviesOfKeyword("Resilience");
  } else if (key == 'w') {
    loadMoviesOfKeyword("Resistance");
  } else if (key == 'e') {
    loadMoviesOfKeyword("Freedom");
  } else if (key == 'r') {
    loadMoviesOfKeyword("Democracy");
  } else if (key == 't') {
    loadMoviesOfKeyword("Revolution");
  } else if (key == 'y') {
    loadMoviesOfKeyword("Change");
  } else if (key == 'u') {
    loadMoviesOfKeyword("Citizen");
  } else if (key == 'd') {
    loadKeywordMovies("Freedom", int(random(movies.length)));
  } else if (key == 'f') {
    loadKeywordMovies("Democracy", int(random(movies.length)));
  } else if (key == 'g') {
    loadKeywordMovies("Revolution", int(random(movies.length)));
  } else if (key == 'h') {
    loadKeywordMovies("Change", int(random(movies.length)));
  } else if (key == 'j') {
    loadKeywordMovies("Citizen", int(random(movies.length)));
  }
}

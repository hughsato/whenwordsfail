void setupKinect() {
  kinect2 = new Kinect2(this);
  kinect2.initDepth();
  kinect2.initDevice();
  depthImg = new PImage(kinect2.depthWidth, kinect2.depthHeight);
  canvas = createGraphics(kinect2.depthWidth, kinect2.depthHeight, P2D);
}

void drawKinect() {
  int[] rawDepth = kinect2.getRawDepth();

  // Threshold the depth image
  for (int i=0; i < rawDepth.length; i++) {
    if (rawDepth[i] >= minDepth && rawDepth[i] <= maxDepth) {
      // raw depth goes up to around 1200
      depthImg.pixels[i] = color(map(rawDepth[i], minDepth, maxDepth, 255, 0));
      //minMax(rawDepth[i]); // for testing
    } else {
      depthImg.pixels[i] = color(0);
    }
  }

  // Draw the thresholded image
  depthImg.updatePixels();
  canvas.beginDraw();
  canvas.background(0);
  canvas.image(depthImg, 0, 0);
  canvas.endDraw();
  //sendToSyphon(canvas);

  tint(255, 255); // must be reset here
  image(canvas, 0, 0, width, height);
}

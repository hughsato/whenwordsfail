Movie[] movies = new Movie[MOVIE_COUNT];
color movieColors[] = new color[MOVIE_COUNT];

int replayTime = 0;
int replayTimeLength = 20000;

// Stores keywords loaded in m1, m2, m3 Movie objects
String keywordsLoaded[] = {"", "", ""};
String VIDEO_PATH = "../videos/";

void setupMovies() {
  for (int i = 0; i < movies.length; i++){
    loadKeywordMovies(keywordDictionary[(int)random(keywordDictionary.length)], i);
  }
}

// rename this function!
void loadReplayM1(String s) {
  // keywordsLoaded[0] = s;
  movies[0] = new Movie(this, VIDEO_PATH + s);
  movies[0].loop();
}

// provide the keyword to randomly find one and which movie to load it to.
void loadKeywordMovies(String k, int whichMov) {
  String[] keywordVideos = getVideosFromDatabase(k);
  color keywordColor = getColorFromDictionary(k);

  // selecting random index from available videos for that keyword
  int l = (int)random(keywordVideos.length);
  logMovieFileLoaded(k, whichMov);

  keywordsLoaded[whichMov] = k;
  movieColors[whichMov] = keywordColor;

  movies[whichMov] = new Movie(this, VIDEO_PATH + keywordVideos[l]);
  movies[whichMov].loop();
}

void loadMoviesOfKeyword(String keyword) {
  for (int i = 0; i < movies.length; i++) {
    movies[i].stop();
    loadKeywordMovies(keyword, i);
    movies[i].play();
  }
}

void drawReplay(int whichMov, int t, String k) {
  color c = getColorFromDictionary(k);
  tint(c, t);
  image(movies[whichMov], 0, 0, width, height);
}

int replayTimer() {
  int timeLeft = replayTime + replayTimeLength - millis();
  println("time left: " + timeLeft);

  if (millis() > replayTime + replayTimeLength) {
    return -1;
  }
  return 0;
}

void loadNewMovie(int whichMov) {
    movies[whichMov].stop();
    movies[whichMov] = null;

    loadKeywordMovies(keywordDictionary[(int)random(keywordDictionary.length)], whichMov);
    movies[whichMov].play();
}

void loadNewMovies() {
    stopReplay();
    for (int i = 0; i < movies.length; i++){
      loadKeywordMovies(keywordDictionary[(int)random(keywordDictionary.length)], i);
    }
    for (Movie mm : movies) {
      mm.play();
    }
}

void stopReplay() {
  for (Movie mm : movies) {
    mm.stop();
    mm = null;
  }
}

void movieEvent(Movie m) {
  m.read();
}

Capture cam;

void setupCamera() {
  String[] cameras = Capture.list();
  printArray(cameras);
  cam = new Capture(this, cameras[9]);
  cam.start();
}

void drawCamera() {
  if (cam.available() == true) {
    cam.read();
  }
  image(cam, 0, 0, width, height);
}

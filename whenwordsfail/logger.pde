void logKeywordColors(int k) {
  println("keywordcolor loaded: " + keywordColors[k]);
}

void logFoundKeyword(boolean b, String str) {
  if (b) {
    println("found: " + str);
  } else {
    println("not found: " + str);
  }
}

void logTableCount(int videoCount) {
  println("We now have " + videoCount + " videos");
}

void logError(String error) {
  println(error);
}

void logMovieFileLoaded(String k, int whichMov){
  println("database loaded: " + k + whichMov);
}

void logMovieState(String state) {
  println(state);
}

void logRecordState(String state) {
  println("ready to record for: " + state);
}

void logEventName(String event) {
  println("got a control event from controller with name " + event);
}

void loadedLog(String loaded) {
  println("loaded: " + loaded);
}

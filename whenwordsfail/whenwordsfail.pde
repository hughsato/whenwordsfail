import processing.video.*;
import com.hamoid.*;
import org.openkinect.processing.*;
import controlP5.*;

// ---- KINECT2 VARIABLES ---- //
Kinect2 kinect2;
PGraphics canvas;
PImage depthImg;
int minDepth = 100;
int maxDepth = 3100;

final int MOVIE_COUNT = 3;
final int RECORD_LENGTH = 10000;
final int FPS = 30;

// Initial SceneState
SceneState sceneState = SceneState.SELECTION;

// ---- RECORDING ---- //
VideoExport videoExport;
String filename = "";

// ---- SETTINGS ---- //
void settings() {
  size(1024, 824);
  // PJOGL.profile=2;
  // fullScreen();
}

// ---- SETUP ---- //
void setup() {
  frameRate(FPS);
  cursor(CROSS);

  interfaceSetup();
  loadVideoDatabase();
  if (KinectConfig.show) setupKinect(); else setupCamera();
  // setupSyphonServer(); // this can only run if kinect2 is running

  setupRecording();
  setupMovies();
}

// ---- DRAW ---- //
void draw() {
  background(0);

  switch(sceneState) {
  case SELECTION:
    tint(0, 0);
    if (KinectConfig.show) drawKinect(); else drawCamera();
    interfaceIsVisible = true;
    drawInterface();
    break;

  // NEXT: eventKeywordSelect() in events.pde
  case READY:
    if (KinectConfig.show) drawKinect(); else drawCamera();
    // TODO: draw "Recording for _______" label

    drawGoButton();
    drawTimer();
    break;

  // NEXT: eventGoSelect() -> beginCapture()
  case RECORDING:
    //  is called by event handler
    if (KinectConfig.show) drawKinect(); else drawCamera();
    drawTimer();

    if (captureFrame() < 0) {
      sceneState = SceneState.REPLAYSINGLE;
      loadReplayM1(lastRecordingFilename);
      replayTime = millis();
    }
    break;

  case REPLAYSINGLE:
    drawReplay(0, 255, keywordsLoaded[0]);
    // also could draw a label for what keyword you're on.

    if (replayTimer() < 0) {
      sceneState = SceneState.SELECTION;
      loadKeywordMovies(latestKeyword, 1);
      replayTime = millis();
      break;
    }
    break;

  case REPLAYDOUBLE:
    for (int i = 0; i < 2; i++){
      drawReplay(i, 200, keywordsLoaded[i]);
    }

    if (replayTimer() < 0) {
      sceneState = SceneState.REPLAYTRIPLE;
      loadKeywordMovies(latestKeyword, 2);
      replayTime = millis();
      break;
    }
    break;

  case REPLAYTRIPLE:
    for (int i = 0; i < movies.length; i++){
      drawReplay(i, 200, keywordsLoaded[i]);
    }
    blendMode(ADD);

    if (replayTimer() < 0) {
      sceneState = SceneState.LIVEPLAY;
      replayTime = millis();
      break;
    }
    break;

  case LIVEPLAY:
    for (int i = 0; i < movies.length; i++){
      drawReplay(i, 200, keywordsLoaded[i]);
    }
    blendMode(ADD);

    tint(255, 255);
    if (KinectConfig.show) drawKinect(); else drawCamera();
    break;

  default:
    println("sceneId does not exist");
    break;
  }

  drawCalibration();
  stroke(0);
}

public void controlEvent(ControlEvent theEvent) {
  String eventName = theEvent.getName();

  // ---- TRIGGERS HENCEFORTH ---- >>
  switch(eventName) {
    case "go":
      eventGoSelect();
      break;

    case "timer":
      break;

    default:
      eventKeywordSelect(eventName);
      break;
  }
}

public void eventGoSelect() {
  // switch to recording mode
  sceneState = SceneState.RECORDING;
  beginCapture();

  hideGo();
  showTimer();
}

public void eventKeywordSelect(String eventName) {
  // All keyword selections go here
  setOutputFilename(eventName);
  logRecordState(eventName);

  sceneState = SceneState.READY;

  hideInterface();
  showGo();
}

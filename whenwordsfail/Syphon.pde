 import codeanticode.syphon.*;

 SyphonServer server;

 void setupSyphonServer() {
   server = new SyphonServer(this, "Processing Syphon");
 }

 void sendToSyphon(PGraphics pg) {
   server.sendImage(pg);
 }
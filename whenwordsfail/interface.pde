ControlP5 cp5;
ControlP5 go;
ControlP5 timer;

final int
SLIDER_WIDTH = 800,
SLIDER_HEIGHT = 100,
BUTTON_WIDTH = 200,
BUTTON_HEIGHT = 70,
BUTTON_START_XPOS = 300,
BUTTON_START_YPOS = 100;

final String
FONT_PATH = "../data/AvenirNext-Bold-40.vlw";

// ---- STYLING ---- //
PFont pfont;
ControlFont font;

// ---- VISIBILITY ---- //
boolean interfaceIsVisible = false;
boolean goIsVisible = false;
boolean timerIsVisible = false;

void interfaceSetup() {
  cp5 = new ControlP5(this);
  go = new ControlP5(this);
  timer = new ControlP5(this);

  // set font
  pfont = loadFont(FONT_PATH);
  font = new ControlFont(pfont,25);

  // keywordDatabase.pde
  setupKeywordVariables();
  setupKeywordButtons();

  go.addButton("go", 0)
    .setColorBackground(#00aa00)
    .setSize(BUTTON_WIDTH, BUTTON_HEIGHT)
    .setPosition(width/2-BUTTON_WIDTH/2, height - 200)
    .setFont(font)
    .setColorForeground(color(255))
    .setColorCaptionLabel(50)
    ;
  go.hide();

  timer.addSlider("timer")
    .setPosition(width/2-SLIDER_WIDTH/2, height-20)
    .setWidth(SLIDER_WIDTH)
    .setRange(0, RECORD_LENGTH)
    .setValue(0)
    .setSize(SLIDER_WIDTH, SLIDER_HEIGHT)
    .setColorForeground(#2ff97d)
    ;
  timer.hide();

  loadedLog("interface");
}

public void createButtons(String w, int i) {
  cp5.addButton(w, i)
    .setPosition(width/2-BUTTON_WIDTH/2,BUTTON_START_YPOS + i*(10+BUTTON_HEIGHT))
    .setColorBackground(keywordColors[i])
    .setSize(BUTTON_WIDTH, BUTTON_HEIGHT)
    .linebreak()
    .setFont(font)
    .setColorForeground(color(255))
    .setColorCaptionLabel(50)
    ;
}

public void drawInterface() {
  if (interfaceIsVisible) {
    interfaceVisibility(true);
  } else {
    interfaceVisibility(false);
  }
}

public void interfaceVisibility(boolean visInt) {
  if (visInt) {
    cp5.show();
  } else {
    cp5.hide();
  }
}

public void showInterface() {
  interfaceIsVisible = true;
  drawInterface();
}

public void hideInterface() {
  interfaceIsVisible = false;
  drawInterface();
}

public void drawGoButton() {
  if (goIsVisible) {
    go.show();
  } else {
    go.hide();
  }
}

public void showGo() {
  goIsVisible = true;
  drawGoButton();
}

public void hideGo() {
  goIsVisible = false;
  drawGoButton();
}

public void drawTimer() {
  if (timerIsVisible) {
    timer.show();
  } else {
    timer.hide();
  }
}

public void hideTimer() {
  timerIsVisible = false;
  drawTimer();
}

public void showTimer() {
  timerIsVisible = true;
  drawTimer();
}

public void updateTimer(int t) {
  timer.getController("timer")
    .setValue(t);
}

void drawCalibration() {
  if (showCalibration && kinect2 != null) {
    fill(255);
    text("THRESHOLD: [" + minDepth + ", " + maxDepth + "]", 10, kinect2.depthHeight);
  }
}

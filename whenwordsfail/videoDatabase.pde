Table table;
int INIT_ARR_LENGTH = 0;

void loadVideoDatabase() {
  // table has headers filename,keyword
  table = loadTable("videoDatabase.csv", "header");
  loadedLog("video database");
}

void addVideoToDatabase(String f, String k) {
  TableRow row = table.addRow();
  row.setString("filename", f);
  row.setString("keyword", k);
  logTableCount(table.getRowCount());

  saveTable(table, "data/videoDatabase.csv");
  loadVideoDatabase();
}

String[] getVideosFromDatabase() {
  // this would return all videos into the string[]
  String[] sr = new String[INIT_ARR_LENGTH];
  for (TableRow row : table.matchRows("*", "keyword")) {
    // println(row.getString("filename"));
    sr = append(sr, row.getString("filename"));
  }
  return sr;
}

// color[] getColorsFromDictionary(String _k) in keywordDatabase.pde
String[] getVideosFromDatabase(String _k) {
  // this returns only videos of a specific keyword
  String[] sr = new String[INIT_ARR_LENGTH];
  for (TableRow row : table.findRows(_k, "keyword")) {
    // println("found " + row.getString("filename"));
    sr = append(sr, row.getString("filename"));
  }
  return sr;
}

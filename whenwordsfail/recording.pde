// ---- Public Variables ---- //
// VideoExport videoExport;
// String filename = "";

int recordTime;
String lastRecordingFilename = "";
String latestKeyword = "";

void setupRecording() {
  videoExport = new VideoExport(this);
  videoExport.setDebugging(true);
  recordTime = 0;
}

void setOutputFilename(String s) {
  lastRecordingFilename =  s + millis() + ".mp4";
  videoExport.setMovieFileName("data/videos/" + lastRecordingFilename);

  // ---- ADD FILE TO DATABASE ---- //
  addVideoToDatabase(lastRecordingFilename, s);
  latestKeyword = s;
}

// ---- START RECORDING ---- //
void beginCapture() {
  videoExport.startMovie();
  recordTime = millis();
  logMovieState("Started movie.");
}

// ---- CAPTURE FRAMES + GOAL CHECK ---- //
int captureFrame() {
  videoExport.saveFrame();
  updateTimer(millis() - recordTime);

  // ---- CASE: STOP RECORDING ----//
  if (millis() > recordTime + RECORD_LENGTH) {
    endCapture();
    return -1;
  }
  return 0;
}

// ---- END RECORDING ---- //
void endCapture() {
  videoExport.endMovie();
  logMovieState("Finished movie.");
  hideTimer();
}

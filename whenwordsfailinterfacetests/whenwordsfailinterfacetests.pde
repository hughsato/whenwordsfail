import controlP5.*;

JSONArray dictionary;
String[] dictKeywords;
ControlP5 cp5;
int buttonBarSize = 400;

void setup() {
  size(1000, 500);

  // initiate controlp5 and create buttonBar
  cp5 = new ControlP5(this);

  // pull in the json directory
  dictionary = loadJSONArray("jsontest/data/dict.json");

  // assign each object into a control button
  dictKeywords = new String[dictionary.size()];
  for (int i = 0; i < dictionary.size(); i++) {
    JSONObject promptObject = dictionary.getJSONObject(i);
    String word = promptObject.getString("keyword");
    String ff = "FF" + promptObject.getString("color").substring(1);

    // add p5 buttons to the list.
    dictKeywords[i] = word;
  }

  // create a button for each entry
  ButtonBar b = cp5.addButtonBar("bar")
    .setPosition(0, 0)
    .setPosition(0, 0)
    .addItems(dictKeywords)
    .setSize(buttonBarSize, 50)
    ;

  b.onRelease(new CallbackListener(){
    public void controlEvent(CallbackEvent ev) {
      ButtonBar bar = (ButtonBar)ev.getController();
      println("hello ",dictKeywords[bar.hover()]);
    }
  });
}

void draw() {
  background(220);
}

// for adding extra items on the fly
void addButtonItem(String s) {
  buttonBarSize += 50;
  ButtonBar bpointer = (ButtonBar)cp5.getController("bar");
  bpointer.addItem(s, 4)
  .setPosition(0, 0)
  .setSize(buttonBarSize, 50);
}

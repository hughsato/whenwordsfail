import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class jsontest extends PApplet {

JSONArray dictionary;

public void setup() {
  
  noLoop();

  dictionary = loadJSONArray("data/dict.json");

  for (int i = 0; i < dictionary.size(); i++) {
    JSONObject promptObject = dictionary.getJSONObject(i);
    String word = promptObject.getString("keyword");
    String ff = "FF" + promptObject.getString("color").substring(1);
    fill(unhex(ff));
    ellipse(random(width), random(height), 10, 10);
  }
}

public void draw() {

}
  public void settings() {  size(100,100); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "jsontest" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}

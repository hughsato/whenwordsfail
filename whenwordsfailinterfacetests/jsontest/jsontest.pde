JSONArray dictionary;

void setup() {
  size(100,100);
  noLoop();

  dictionary = loadJSONArray("data/dict.json");

  for (int i = 0; i < dictionary.size(); i++) {
    JSONObject promptObject = dictionary.getJSONObject(i);
    String word = promptObject.getString("keyword");
    String ff = "FF" + promptObject.getString("color").substring(1);
    fill(unhex(ff));
    ellipse(random(width), random(height), 10, 10);
  }
}

void draw() {

}
